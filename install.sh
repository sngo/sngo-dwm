#!/bin/bash
PWD=$(pwd)
OLD_PWD=$PWD
PATCHES=$PWD/patches
FINAL_PATCHES=$PWD/final_patches
set -euo pipefail
# echo "==== Updating Submodule ==== "
# cd $PWD/dwm
# git reset origin/HEAD --hard
# make clean
# cd $OLD_PWD
# git submodule init
# git submodule update
cd $PWD/dwm
if [ -f "$OLD_PWD/config.h" ]; then
	set +e
	git branch -D config
	git checkout -b config
	retval=$?
	set -e
	echo "==== Copying config file ==== "
	cp $OLD_PWD/config.h $OLD_PWD/dwm/
	git add config.h
	git commit -am "Copying config"
fi
git checkout submo
echo "==== Creating Patch Branches ==== "
cd $OLD_PWD/dwm
for patch in $(ls $PATCHES); do
	echo "==== Creating branch for $patch ==== "
	set +e
	git branch -D $patch
	git checkout -b $patch
	retval=$?
	set -e
	if [[ ! $retval -eq 128 ]]; then
		git apply -3 --ignore-whitespace $PATCHES/$patch
		git commit -am "Applying $patch"
		git checkout submo
	fi
done
for patch in $(ls $PATCHES); do
	echo "==== Installing $patch ==== "
	set +e
	git merge $patch -m "Merging $patch"
	retval=$?
	set -e
	if [[ ! $retval -eq 0 ]]; then
		for file in $(git status | grep both | awk '{ print $3 }'); do
			sed -i '/^\(<<<<<<\|=======\|>>>>>>>\)\(.\)*$/d' $file
			git add $file
		done
		git commit -am "Merge fix $patch"
	fi
done
echo "applying final patches"
for patch in $(ls $FINAL_PATCHES); do
	echo "==== Final patch $patch ==== "
	git apply -3 --ignore-whitespace $FINAL_PATCHES/$patch
done
echo "==== Compiling and installing ==== "
cp $OLD_PWD/config.h $OLD_PWD/dwm/
sudo make clean install
cd $OLD_PWD
