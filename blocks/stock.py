#!/usr/bin/python
import yfinance as yf;
import argparse;
import decimal;
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
#   ENDC = '\033[0m'
    ENDC = ''
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

parser = argparse.ArgumentParser(description='Enter stock to watch')
parser.add_argument('stock', metavar='AMZN')
args = parser.parse_args();

ticker = yf.Ticker(args.stock);
info = ticker.info
bid = ticker.info["bid"]
open_price = ticker.info["open"]
percent = (bid - open_price)/open_price*100

if (bid - open_price > 0):
    color = bcolors.OKGREEN
    color = '+'
else:
    color = bcolors.FAIL
    color = '-'

print(color + args.stock + ": " + str(bid) + " (" + "{:.2f}".format(percent)  + "%)" + bcolors.ENDC);
