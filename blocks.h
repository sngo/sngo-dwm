//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "~/sngo-dwm/blocks/stock.py PLAY",	600,		0},
	{"", "~/sngo-dwm/blocks/stock.py AMZN",	600,		0},
	{"", "~/sngo-dwm/blocks/volume",	1,		10},
	{"", "~/sngo-dwm/blocks/wifi",	30,		10},
	{"", "~/sngo-dwm/blocks/battery",	600,		0},
    {"", "~/sngo-dwm/blocks/clock",						5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
