#!/bin/bash
PWD=$(pwd)
OLD_PWD=$PWD
PATCHES=$PWD/dwmblocks_patches
set -euo pipefail
# echo "==== Updating Submodule ==== "
# cd $PWD/dwmblocks
# git reset origin/HEAD --hard
# make clean
# cd $OLD_PWD
# #git submodule init
# #git submodule update
cd $PWD/dwmblocks
if [ -f "$OLD_PWD/blocks.h" ]; then
	set +e
	git branch -D config
	git checkout -b config
	retval=$?
	set -e
	echo "==== Copying config file ==== "
	cp $OLD_PWD/blocks.h $OLD_PWD/dwmblocks/
	git add -f blocks.h
	git commit -am "Copying config"
fi
git checkout submo
echo "==== Creating Patch Branches ==== "
cd $OLD_PWD/dwmblocks
for patch in $(ls $PATCHES); do
	echo "==== Creating branch for $patch ==== "
	set +e
	git branch -D $patch
	git checkout -b $patch
	retval=$?
	set -e
	if [[ ! $retval -eq 128 ]]; then
		git apply -3 --ignore-whitespace $PATCHES/$patch
		git commit -am "Applying $patch"
		git checkout submo
	fi
done
for patch in $(ls $PATCHES); do
	echo "==== Installing $patch ==== "
	set +e
	git merge $patch -m "Merging $patch"
	retval=$?
	set -e
	if [[ ! $retval -eq 0 ]]; then
		for file in $(git status | grep both | awk '{ print $3 }'); do
			sed -i '/^\(<<<<<<\|=======\|>>>>>>>\)\(.\)*$/d' $file
			git add $file
		done
		git commit -am "Merge fix $patch"
	fi
done
echo "==== Compiling and installing ==== "
cp $OLD_PWD/blocks.h $OLD_PWD/dwmblocks/
sudo make clean install
cd $OLD_PWD
